[name] New design 1
[puzzle] Sz048
[production-cost] 900
[power-usage] 423
[lines-of-code] 15

[traces] 
......................
......................
......................
......................
......................
..95555555555555555C..
..A95555C.9555555416..
..2A...16.A.......14..
..1694.15574.15C..14..
..15755555C....A......
..........34.1C35554..
..............34......
......................
......................

[chip] 
[type] DIAL7
[x] 15
[y] 2
[is-puzzle-provided] true

[chip] 
[type] UC6
[x] 11
[y] 3
[code] 
  teq 001 x0
+ mov x2 acc
  
  teq x0 010
+ teq p0 100
+ tgt acc 0
+ mov 100 p1
+ slp 1
+ mov 0 p1
+ sub 1
  
  slp 1

[chip] 
[type] UC4
[x] 5
[y] 5
[code] 
  teq 100 p1
+ mov 100 p0
  
  teq 100 x1
+ mov 0 p0
  
  slp 1

[chip] 
[type] DX3
[x] 17
[y] 5

